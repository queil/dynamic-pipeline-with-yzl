#r "nuget: Yzl, 2.0.0"
#r "nuget: Fake.Tools.Git, 6.0.0"

open Fake.Tools.Git
open System
open Yzl

let pwd = IO.Directory.GetCurrentDirectory()
let dirs = 
  FileStatus.getAllFiles pwd
  |> Seq.map (snd >> IO.FileInfo >> fun f -> IO.Path.GetRelativePath(pwd, f.Directory.FullName))
  |> Seq.filter ((<>) ".")
  |> Seq.distinct

let stage = Yzl.str
let image = Yzl.str
let script = Yzl.str<Str>

for d in dirs do
  
    [
        $"pipeline-%s{d}" .= [
            stage "build"
            image "mcr.microsoft.com/dotnet/sdk:7.0"
            script !>-
              $"""
               dotnet build "%s{d}"

               """
        ]
    ] |> Yzl.render |> Console.Write
